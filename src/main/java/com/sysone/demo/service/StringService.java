package com.sysone.demo.service;

import org.springframework.stereotype.Service;

import lombok.Data;

@Data
@Service
public class StringService {
	
	
	
	public  String compressString(String cadena) {
	      int countConsecutive = 0;
	      for (int i = 0; i < cadena.length(); i++) {
	        countConsecutive++;
	        /* Si el siguiente carácter es diferente al actual, agrega este carácter al resultado. */
	        if (i + 1 >= cadena.length() || cadena.charAt(i) != cadena.charAt(i + 1)) {
	        	cadena = cadena.replaceFirst(cadena.substring(i+1-countConsecutive, i+1), countConsecutive+""+cadena.charAt(i));
	            i=i-countConsecutive+1+(countConsecutive+"").length();
	            countConsecutive = 0;
	        }
	      }
	      return cadena;
	}

}
