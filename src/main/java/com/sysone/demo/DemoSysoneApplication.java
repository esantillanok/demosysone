package com.sysone.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class DemoSysoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSysoneApplication.class, args);
	}

}
