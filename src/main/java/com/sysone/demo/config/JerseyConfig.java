package com.sysone.demo.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.sysone.demo.rest.SysOneServiceRest;


@Configuration
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(SysOneServiceRest.class);
		
	}
}
