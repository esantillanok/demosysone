package com.sysone.demo.model;

import lombok.Data;

@Data
public class DataResponse {
	
	private String compressed;

}
